package ifrs.canoas.job.notepadhelper;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dessa.notepadhelper.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import ifrs.canoas.job.notepadhelper.lib.DatabaseHelper;
import ifrs.canoas.job.notepadhelper.model.Note;

public class NotepadActivity extends AppCompatActivity {

    private Note note = new Note();
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notepad);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbHelper = new DatabaseHelper(getApplicationContext());
        TextView notasAdicionadas = (TextView) findViewById(R.id.tvNotas);
        if (Note.getAll(dbHelper) != null)
            notasAdicionadas.setText(note.listToString(Note.getAll(dbHelper)));
    }

    public void save(View v) {
        EditText title = (EditText) findViewById(R.id.txtTitulo);
        EditText text = (EditText) findViewById(R.id.txtDescricao);
        EditText discipline = (EditText) findViewById(R.id.txtDisciplina);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        note.setDate(sdf.format(new Date()));
        note.setDiscipline(discipline.getText().toString());
        note.setText(text.getText().toString());
        note.setTitle(title.getText().toString());
        if (!(title.getText().toString().isEmpty() || text.getText().toString().isEmpty() || discipline.getText().toString().isEmpty()))
            note.insert(dbHelper);
        title.setText("");
        text.setText("");
        discipline.setText("");
        //Log.d("DEBUG", "Notas " + Note.getAll(dbHelper).toString() + " " + Note.getAll(dbHelper).size());
        TextView notasAdicionadas = (TextView) findViewById(R.id.tvNotas);
        notasAdicionadas.setText(note.listToString(Note.getAll(dbHelper)));
    }

    public void list(View v) {

    }

}
