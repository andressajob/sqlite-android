package ifrs.canoas.job.notepadhelper.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import ifrs.canoas.job.notepadhelper.lib.DatabaseHelper;
import ifrs.canoas.job.notepadhelper.lib.NotepadContract;

/**
 * Created by dessa on 18/09/2017.
 */

public class Note {

    private int idNote;
    private String title;
    private String date;
    private String text;
    private String discipline;

    public Note() {
    }

    public Note(int idNote, String title, String date, String text, String discipline) {
        this.idNote = idNote;
        this.title = title;
        this.date = date;
        this.text = text;
        this.discipline = discipline;
    }

    public long insert(DatabaseHelper databaseHelper) {
        // Gets the data repository in write mode
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(NotepadContract.FeedEntry.COLUMN_NAME_TITLE, title);
        values.put(NotepadContract.FeedEntry.COLUMN_NAME_DATE, date);
        values.put(NotepadContract.FeedEntry.COLUMN_NAME_DISCIPLINE, discipline);
        values.put(NotepadContract.FeedEntry.COLUMN_NAME_TEXT, text);

        // Insert the new row, returning the primary key value of the new row
        return db.insert(NotepadContract.FeedEntry.TABLE_NAME, null, values);
    }

    public long update(DatabaseHelper databaseHelper) {
        // Gets the data repository in write mode
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(NotepadContract.FeedEntry.COLUMN_NAME_TITLE, title);
        values.put(NotepadContract.FeedEntry.COLUMN_NAME_DATE, date);
        values.put(NotepadContract.FeedEntry.COLUMN_NAME_DISCIPLINE, discipline);
        values.put(NotepadContract.FeedEntry.COLUMN_NAME_TEXT, text);

        String select = NotepadContract.FeedEntry._ID + "= ?";
        String[] arguments = {String.valueOf(this.idNote)};
        return db.update(NotepadContract.FeedEntry.TABLE_NAME, values, select, arguments);
    }

    public void delete(DatabaseHelper databaseHelper) {
        // Gets the data repository in write mode
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String select = NotepadContract.FeedEntry._ID + "= ?";
        String[] arguments = {String.valueOf(this.idNote)};
        db.delete(NotepadContract.FeedEntry.TABLE_NAME, select, arguments);
    }

    public void load(DatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                NotepadContract.FeedEntry._ID,
                NotepadContract.FeedEntry.COLUMN_NAME_TITLE,
                NotepadContract.FeedEntry.COLUMN_NAME_DATE,
                NotepadContract.FeedEntry.COLUMN_NAME_TEXT,
                NotepadContract.FeedEntry.COLUMN_NAME_DISCIPLINE
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = NotepadContract.FeedEntry._ID + " = ?";
        String[] selectionArgs = {String.valueOf(this.idNote)};

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                NotepadContract.FeedEntry.COLUMN_NAME_TITLE + " DESC";

        Cursor c = db.query(
                NotepadContract.FeedEntry.TABLE_NAME,     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
        c.moveToFirst();
        this.idNote = c.getInt(c.getColumnIndex(NotepadContract.FeedEntry._ID));
        this.title = c.getString(c.getColumnIndex(NotepadContract.FeedEntry.COLUMN_NAME_TITLE));
        this.text = c.getString(c.getColumnIndex(NotepadContract.FeedEntry.COLUMN_NAME_TEXT));
        this.discipline = c.getString(c.getColumnIndex(NotepadContract.FeedEntry.COLUMN_NAME_DISCIPLINE));
        this.date = c.getString(c.getColumnIndex(NotepadContract.FeedEntry.COLUMN_NAME_DATE));
    }

    public static List<Note> getAll(DatabaseHelper databaseHelper) {
        LinkedList<Note> list = new LinkedList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                NotepadContract.FeedEntry._ID,
                NotepadContract.FeedEntry.COLUMN_NAME_TITLE,
                NotepadContract.FeedEntry.COLUMN_NAME_DATE,
                NotepadContract.FeedEntry.COLUMN_NAME_TEXT,
                NotepadContract.FeedEntry.COLUMN_NAME_DISCIPLINE
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                NotepadContract.FeedEntry.COLUMN_NAME_TITLE + " DESC";

        Cursor c = db.query(
                NotepadContract.FeedEntry.TABLE_NAME,     // The table to query
                projection,                               // The columns to return
                null,                                     // The columns for the WHERE clause
                null,                                     // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                list.add(new Note(
                        c.getInt(c.getColumnIndex(NotepadContract.FeedEntry._ID)),
                        c.getString(c.getColumnIndex(NotepadContract.FeedEntry.COLUMN_NAME_TITLE)),
                        c.getString(c.getColumnIndex(NotepadContract.FeedEntry.COLUMN_NAME_DATE)),
                        c.getString(c.getColumnIndex(NotepadContract.FeedEntry.COLUMN_NAME_TEXT)),
                        c.getString(c.getColumnIndex(NotepadContract.FeedEntry.COLUMN_NAME_DISCIPLINE))));
            } while (c.moveToNext());
        }
        return list;
    }

    public int getIdNote() {
        return idNote;
    }

    public void setIdNote(int idNote) {
        this.idNote = idNote;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    @Override
    public String toString() {
        return "Title: " + title + "\tDate: " + date + "\tText: " + text + "\tDiscipline: " + discipline + "\n";
    }

    public String listToString(List<Note> list){
        String result = "";
        for (Note note : list) {
            result += note.toString() + "\n";
        }
        return result;
    }
}